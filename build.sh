#!/bin/bash
rm $1.html

title=$(head -n 1 $1.markdown | cut -c 2-)

cat header.html >> $1.html

markdown $1.markdown >> $1.html

cat footer.html >> $1.html

sed -i "s|--|\&mdash;|g" $1.html

sed -i "s|%TITLE%|$title|g" $1.html
