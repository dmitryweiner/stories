# Курс выживания туриста в городской тайге

_"Очень немного требуется, чтобы уничтожить человека: стоит лишь убедить его в том,
что дело, которым он занимается, никому не нужно."_

## Введение

Судьба человека -- вещь непредсказуемая по определению. Вот он сидит спокойно
у костра, никого не трогает, попивает из жестяной кружки пахучий травяной чай и наслаждается
собственным существованием. А вот он уже мчится зачем-то в серый унылый офис в тщетной
попытке добыть из реальности побольше цветных бумажек. Оставим в стороне вопрос, зачем
он это делает. Давайте лучше поможем ему не погибнуть в этом безжалостном мире из
стекла и бетона. А помогут ему в этом советы бывалых охотников, не раз попадавших
в каменный ад и с успехом из него выходивших.

Данное руководство подготовлено с учётом опыта выживания многих уважаемых людей и 
содержит только проверенную информацию. Возможно, когда-нибудь она спасёт вам жизнь.

## Добыча еды

Критически важно помнить, что тело человеческое сделано по образу и подобию обезьян, 
а значит, многие его функции завязаны на регулярную физическую активность. Кишечник 
такого существа привык есть много грубой пищи (клетчатки). Что же делает горожанин? 
Сидит на пятой точке и поедает полупереваренные продукты (манную кашу, колбасу, конфеты). 
Это не может не сказаться на составе кишечной микрофлоры, которая, как выяснилось, 
влияет на поведение [[1]](#1), например, аномальная микрофлора связана с повышенной частотой 
депрессии [[2]](#2). Дабы не уподобляться чахлому горожанину, следует каждый, я подчёркиваю, 
каждый день употреблять в пищу клетчатку и молочнокислые продукты. Точно так же следует 
каждый день заниматься хоть какой-то физической активностью (хорошо помогает проходить 
минимум 5 км пешком).

Несмотря на внешнее обилие пищи, есть горожанину практически нечего. Вместо нормальной 
удобоваримой еды предлагаются ему сплошь эрзацы, вкуснопахнущие и обильно сдобренные 
заменителями вкуса.

Захочет, например, городской житель мяса (есть такое желание, человек же всеяден). 
А ему пихают в нос колбасу или сосиски. Читаем состав: текстурированная соя, вода, 
соль, нитрит натрия, молотые хрящи, крысиные хвосты. И лишь в конце списка сиротливо 
ютится мясо.

То же можно сказать и про остальное. Попробуйте зайти в обычную пролетарскую столовую 
и поесть там. У вас будет ощущение, будто бы вы выпили слишком много воды из помойной ямы. 
Места, где готовят нормальную вкусную еду, считаются очень статусными и называются ресторанами.

Не стоит есть котят, предлагаемых на станциях метро. Во первых, на вас в лучшем случае
косо посмотрят. Во вторых, котята эти тощие и невкусные, т.к. предлагающие их бабки
совершенно не заботились об их благополучии и кормили самой дешёвой кошачьей едой (если
кормили). Как не стоит есть и привокзальные беляши, приготовленные из тех же котят. Во первых,
из них торчат котовьи усы. Во вторых и остальных, это просто невкусно.

Что же делать обычному человеку, вдруг решившему поесть? 
Всё просто, надо сделать себе еду самостоятельно. 
Отталкиваться следует от того, какие продукты есть в наличии, чем мы их можем обработать 
(инструменты), от наличия и типа пищевого реактора (кастрюля, сковорода, горшок, чайник 
для варки пельменей, etc.). Крайне важно то, на чём мы собираемся варить. Вкуснее всего 
еда получается у русской печи. Далее по нисходящей: газ, электричество, микроволновка.

За детальными (порою слишком) инструкциями отправляю читателя к Вильяму нашему Похлёбкину [[3]](#3)
или к "Поваренной книге нищеброда" (есть аналогичная про анархиста, не особо интересная). 
А для тех, кому лень искать эту книгу, привожу цитаты, говорящие сами за себя.

> "Вкусная еда оставляет положительные воспоминания, добрые эмоции. Невкусная еда, 
> даже если её избыток, либо не оставляет ничего в памяти о себе, либо содействует 
> аккумуляции отрицательных ассоциаций. Отсюда видно, что и ароматически-вкусовое 
> качество пищи, а не только санитарно-пищевое, которое обычно учитывается, имеет 
> исключительно важное значение в жизни человека." В. Похлёбкин.

> "Даже шедевры кулинарии не могут быть сохранены ни в каких музеях. Они съедаются 
> тем быстрее, чем они прекраснее."

> "Именно исходя из этого, птицу перед помещением в фольгу прочно увязывают, 
> сшивают, чтобы сделать её неподвижной."

> "Кроме того, суп – блюдо оседлого человека. И он остаётся таким до наших дней. 
> Только в прочной, постоянной семье едят суп регулярно. Отсутствие супа в доме – 
> один из первых показателей и признаков семейного неблагополучия.

> Вот почему сделать суп дано не каждому. Его может сделать человек, не только 
> знающий, точнее, многознающий, но и спокойный, уравновешенный, уверенный, 
> стабильный по своему нраву, по своей психике и в то же время не лишённый 
> творческой жилки, кулинарной и общей одарённости. Вот, оказывается, сколько 
> качеств надо иметь, каким комплексом их надо обладать, чтобы приготовить 
> «простой суп». 

> «Не много ли чести?» – спросит иной читатель. 

> Нет. Не много. «Простой суп» вовсе не простое дело. Вернее, если суп 
> получается простой, то это не суп, его лучше не делать."

Или же можно обратиться к классике:

> -- Еда, Иван Арнольдович, штука хитрая. Есть нужно уметь, и, представьте себе, 
> большинство людей вовсе этого не умеет. Нужно не только знать, что съесть, но и когда и как. 
> (Филипп Филиппович многозначительно потряс ложкой.) И что при этом говорить. Да-с. Если вы 
> заботитесь о своем пищеварении, вот добрый совет - не говорите за обедом о большевизме 
> и о медицине. И, боже вас сохрани, не читайте до обеда советских газет.

> -- Гм... Да ведь других нет?

> -- Вот никаких и не читайте. Вы знаете, я произвел тридцать наблюдений у себя в клинике. 
> И что же вы думаете? Пациенты, не читающие газет, чувствовали себя превосходно. 
> Те же, которых я специально заставлял читать "Правду", теряли в весе.

> -- Гм... - с интересом отозвался тяпнутый, розовея от супа и вина.
 
> -- Мало этого. Пониженные коленные рефлексы, скверный аппетит, угнетенное состояние духа.
 
> -- Вот черт...

> -- Да-с. Впрочем, что же это я! Сам же заговорил о медицине. Будемте лучше есть. 

## Перемещение

_"Нас невозможно сбить с пути, нам всё равно, куда идти."_

Навигация в городе осуществляется посредством штатных систем, имеющихся у 
любого туриста: навигатора или КПК, оснащённого приёмником GPS. Другое дело, 
что карты в городе устаревают гораздо быстрее, поэтому их нужно хотя бы 
ежемесячно обновлять.

С помощью свежих карт можно получить доступ к еде, узнать о новых маршрутах 
или найти пригодные для обитания места. Обязательно позаботьтесь о навигации 
перед приездом в новый незнакомый город. Без карты вы будете подобны жалкому 
горожанину, спрашивающему дорогу у разных случайных людей (автор не раз по 
ошибке направлял таких вопрошающих в диаметрально противоположную сторону).

Искусство чтения следов пригождается не только в тайге, но и в городе. Обычно магазин оснащается несколькими 
входами для увеличения пропускной способности. Но лишь один из них бывает открыт. Так делают согласно 
труднообъяснимым, магическим принципам (полагаю, это связано с тем, что если отверстий много, злым 
духам будет легче попасть внутрь). Опытный следопыт легко определит, какая из дверей активна, 
по мокрым или снежным следам перед дверью или же по поведению идущих впереди него людей.

Хорошо, если в городе существует система отслеживания положения транспорта. 
Без этой системы путешественник опять же превращается в робкое существо, 
спрашивающее вагоновожатую: "Из-из-вините, вы по-по-по маршруту?" Обладающий 
же соответствующей информацией таёжник подходит к остановке как хозяин (и не 
в паразитологическом смысле, прошу заметить) и решительно садится в нужный 
ему вагон. Можно даже веско заметить: "Опаздываете, судари, в 18:51 должны 
были быть!"

Все транспортные средства можно ранжировать по быстроте и беспроблемности 
работы. Лидирует в этом рейтинге метро. Оно ходит регулярно, не подвержено 
пробкам, не встаёт при ДТП и не зависит от текущей погоды.

Далее следует трамвай. К сожалению, многие недооценивают этот вид транспорта, 
а зря. Бесчётное число раз проезжал я на отчаянно вихляющем и громыхающем 
трамвае мимо глухой пробки. Однако, есть тут и ложка дёгтя: столновение 
двух железных одиночеств на путях может нарушить всё тщательно выстроенное 
расписание.

Что же делать в этом случае? Пересаживаться на троллейбус! Обладая всеми 
преимуществами трамвая, троллейбус тем не менее вынужден стоять в пробках. 
Зато он не смердит, как нижеследующем представители механического племени.

Автобусы. Когда-то они были кра сивыми и грациозными механизмами, издававшими 
мелодичный и приятный для слуха бутылочный звон (ЛиАЗ-677) и подкидывавшими 
на задней площадке визжащую малышню. Но времена изменились. Теперь это угрюмые 
чадящие коробки, стоящие на каждой остановке по полчаса в ожидании хоть 
каких-нибудь пассажиров. Лучше не будем о них.
 
Как не будем и о карликовой разновидности автобусов -- маршрутках. Что может 
быть алчнее водителя маршрутки, шакалящего перед беззащитным медлительным 
автобусом (они это называют "поехать под сарай", потрясающая циничность) и 
везущего своих наивных пассажиров за двойную,  а то и за тройную цену.

Словом, выбирайте рельсовый электротранспорт и не прогадаете.

Не стоит упускать из виду и вопрос экипировки. В городе физико-химические условия 
среды порой даже разнообразнее, чем на природе. Вы можете в один и тот же день 
пройтись по сухому пыльному асфальту, снежной кашице и абсолютно гладкому льду. 
Понятно, что никакая городская обувь это не выдержит. Только трекинговая со 
специальной подошвой, с мембраной, обработанная водоотталкивающей пропиткой. 
То же можно сказать и об одежде. Она должна легко подстраиваться под текущие нужды.
Скажем, с утра было -17, днём воздух прогрелся до +5, а ночь вы проводите не дома. 
Поэтому только термобелье, только хардкор. Куртка должна быть такая, чтобы в ней 
можно было просто лечь на бок и заснуть.

Ну и конечно же рюкзак. Жизнь в городе без него совершенно невозможна. Куда сложить 
добытые с таким трудом припасы? А где хранить запасные свитера, шапки, носки на 
случай промокания? И куда складывать фонари, навигаторы и прочие полезные девайсы? 
Всё туда, в рюкзак.

Таким образом мы видим, что экипировка в городе, пожалуй, даже ещё более важна, 
чем в диком лесу.

### Семён Голубицкий и его волны

![Схема распространения волн](illustrations/city-life/golub.jpg "Схема распространения волн")

_Рис. 1. Схема распространения волн._

Затрагивая тему путешествий на общественном транспорте, нельзя не упомянуть 
труды выдающегося учёного Семёна Голубицкого [[4]](#4), описавшего волны плотности 
людского потока.

Остановимся на этом явлении поподробнее. Голубицкий говорит о трёх волнах 
плотности, предшествующий появлению электропоезда метро. Первая волна со 
сравнительно слабой амплитудой накатывает в тот момент, когда пассажиры 
слышат из тоннеля звук прибывающего поезда. Вторая волна распространяется 
синхронно с прибывающим поездом. Если смотреть сверху, она напоминает след 
катера на гладкой воде. Ну и, наконец, третья волна Голубицкого, самая 
мощная по амплитуде, обрушивается на поезд в момент открытия дверей.

Что же делать во время всего этого непредвзятому наблюдателю. Во первых, 
надо сохранять спокойствие. Если вдруг вам показалось, что поезд уедет 
без вас, то он не уедет. Если вами вдруг овладело яростное желание захватить 
себе место, успокойтесь. За время езды вы навряд ли успеете так устать, 
чтобы место потребовалось.

Пытливый читатель, вооружившись хронометром и блокнотом, может попытаться 
выявить корреляцию между плотностью особей, временем суток и особенно 
солнечной активностью. Голубицкий так и не опубликовал свои дневники, 
а ведь в них был ключ к этому загадочному и грозному явлению.

## Как разбить лагерь в городе

![Жилище Игоря Грудцинова](illustrations/city-life/3.jpg "Жилище Игоря Грудцинова")

_Рис. 2. Жилище Игоря Грудцинова._

Бывалому туристу не престало жить подобно голубю. Он хочет спать в теплой сухой постели, 
защищённой от взглядов посторонних. В нормальных условиях турист, например, ставит палатку, 
роет землянку или, на худой конец, ставит избу.

Место для стоянки следует выбирать тщательно и с умом. Так, например, нельзя просто по 
привычке раскинуть палатку и просто начать жить в любой понравившейся точке планеты. Скорее 
всего, это закончится взаимодействием с государственной машиной в лице бравых сержантов 
полиции. Нет, горожанин с детства выдрессирован, что просто так жить в понравившихся 
местах нельзя. 

![Жилище бедуинов](illustrations/city-life/5.jpg "Жилище бедуинов")

_Рис. 3. Жилище бедуинов._

Во-первых, всегда нужно доказывать своё право на жизнь в определённом месте. Это у городских 
жителей называется пропиской или регистрацией. Когда индивидуальный идентификатор особи 
записывается в специальную большую книгу, особь чувствует, что теперь-то она имеет право жить.

Во-вторых, нельзя нигде жить бесплатно. Обязательно нужно кому-то отдавать кусочки 
раскрашенной бумаги, которые все договорились считать ценными. Чтобы добыть эти самые 
кусочки, люди порой идут на страшные вещи, например, придумывают, как бы побольнее 
уколоть своих собратьев, дабы вызвать у них острое, болезненное желание заработать 
ещё бумажек.

В третьих, считается очень неприличным частая смена места обитания. Человек не может 
просто взять и переехать, если ему надоел пейзаж в окне. Обязательно нужна веская 
причина: чья-нибудь смерть, предложили больше бумажек в другом городе, обида на 
несуществующие вещи типа государства. Нельзя уехать из-за желания посмотреть, 
как же выглядит наша планета в других местах.

Человек, склонный к перемене местообитания считается плохим, ненадёжным. С таким 
не будут вести дела и брать на работу (это и к лучшему).

Что же делать в этом случае туристу? Вариантов несколько:

Поскольку палатку где угодно поставить нельзя, можно было бы поставить каменный дом 
по примеру городских жителей. К сожалению, это сопряжено с таким огромным количеством 
бессмысленных действий, что проще отказаться на начальном этапе.

### Ипотека

![Жилище таёжника](illustrations/city-life/2.jpg "Жилище таёжника")

_Рис. 4. Жилище таёжника._

Следующим по сложности квестом является ипотека.
Слово берёт своё начало 
от греческого _hupotithenai_ помещать, подставлять под _hypo_- снизу + _tithenai_ помещать = подставка, подпорка.
Так назывался столб на границе земли, которую надо будет отдать в счёт долга.

Напрасно ругают ипотеку. Это очень выгодное предприятие, если:

* Вы уверены, что не заболеете серьёзно.

* Вы уверены, что вас не уволят в ближайшие N лет.

* Что даже если и уволят, то вы легко найдёте работу.

* Все подходящие работы находятся на приемлемом расстоянии от жилища.

* Обладание имуществом приятно греет душу (по чужим углам не скитаюсь, чужому дяде не плачу).

* Нет планов пожить где-то ещё. В принципе, довольно реально рефинансировать ипотеку и 
купить какой-нибудь ещё дом, но это сопряжено с геморроем размером с кулак.

### Аренда

![Жилище маори](illustrations/city-life/1.jpg "Жилище маори")

_Рис. 4. Жилище маори._

Если же всё вышеперечисленное вам не подходит, выбираем квест попроще. Это -- аренда. 
Здесь тоже существуют свои хитрости. Опытные таёжники наверное уже поняли, о чём я. 
Да, бывают квартиры, в которых долго не проживёшь. Там может быть хороший ремонт и 
дорогая сантехника, но почему-то жильцы в них не задерживаются. Таких квартиры стоит 
избегать.

Распознать их довольно просто. Воздух в них всегда слегка затхлый и пахнет отчаянием, 
как будто в них долго и безутешно плакали. Словом, если вы это почувствовали, уходите 
оттуда сразу же.

В хорошей же квартире хочется сползти по стеночке и блаженно лечь уже прямо в коридоре 
(у автора был такой случай во время доставки холодильника, что имело потом далеко 
идущие последствия). А уходить из таких мест совсем не хочется. Такую квартиру нужно 
долго и вдумчиво искать. Запускать её тоже не следует, ведь она может испортиться.

Важный нюанс -- это канал поиска квартир. Самый плохой способ искать квартиру через 
риэлтора. Данный бесполезный человек зачем-то возьмёт с вас деньги за доступ к базе 
данных. Да вы и сами можете так же искать. Лучшие варианты попадаются на локальных 
форумах или социальных сетях. По-прежнему работают бумажные объявления. Но лучше 
всего искать квартиру через сарафанное радио. Такие варианты, как правило, самые 
уютные и долговечные.

### Хостел

Если же вы решили, что и этот квест слишком сложен для вас, можно пожить в хостеле. 
Дальнейшие варианты зависят от объема крашеной бумаги, которым вы располагаете. 
Если денег не жалко, можно поселиться в отдельной комнатушке (это ненамного дешевле 
гостиницы). Если же вы решили сэкономить, жить можно в большой комнате с несколькими 
особями. В этом случае незаменимыми атрибутами являются: тёмная повязка на глаза для 
защиты от света экранов и беруши для защиты от неизбежного храпа. Несмотря на кажущуюся 
невыносимость, жить так можно очень долго, да практически всю жизнь.

### Теплотрасса

![Жилище](illustrations/city-life/4.jpg "Жилище")

_Рис. 4. Жилище._

Если же и этот вариант вам не подходит, можно жить в необитаемых местах: в подвалах, 
на теплотрассе. К сожалению, в этом случае вам придётся с толкнуться с жёсткой, 
я бы даже сказал, жестокой конкуренцией с особями своего вида. Эти бывалые туристы помоек, 
вагабонды песчаных карьеров начисто лишены каких-либо моральных принципов и не брезгуют 
вяленой человечиной (а уж от копчёной крысятины их и подавно не оттащишь). Лишь сильные 
духом способны добыть себе место подле тёплой трубы отопления.

## Флора и фауна городов

Средний горожанин думает, что в бетонных джунглях живёт только один вид, Homo sapiens (последнее под вопросом). 
Изредка такой горожанин с раздражением замечает вокруг себя представителей синантропной фауны: кошек, собак, 
воробьёв и вездесущих (не только) голубей. Но реальность на проверку оказывается сложнее и страшнее.

Например, в метро обитает самоподдерживающаяся популяция тропических комаров. Учитывая, что они кусают 
не только людей, но и гигантских метрокрыс, можно представить, какой букет болезней получает укушенный.

Те же собаки давно шагнули за границы разумности своего вида и с успехом ездят на общественном транспорте 
(автор воочию наблюдал, как его знакомая собака Муся ездила на автобусе №23 с Цветного проезда до мясного 
рынка в Щ и очень смущалась, будучи уличённой).

Мало кто замечает так называемую крипто-фауну, в частности, бетонных червей. Эти существа обладают весьма 
занимательным циклом развития. Личинки бетонных червей живут в деревянной мебели, именно так они попадают 
в новые дома. Когда личинка немного повзрослеет, она вгрызается в стену. Благодаря специфическому метаболизму, 
черви могут поедать камень, бетон, кирпичи и даже гипсокартон. Когда приходит пора размножения, самцы червей 
в определенном ритме сверлят бетон, привлекая самок. Пик их активности приходится на утро выходного дня. 
Поэтому, прежде чем идти к соседям разбираться, протравите в своём доме бетонных червей.

Нельзя обойти вниманием вопрос происхождения бомжей, пакетов, ночных бабочек и голубей.

Да-да, именно так, мой дорогой читатель. Все эти существа суть звенья одной цепи. Рассмотрим 
их жизненный цикл поподробнее.

Как известно, самозарождение жизни не только возможно, но и неизбежно. Учёные 
продемонстрировали образование аминокислот в специально подобранной среде, имитирующей 
первичный бульон [[5]](#5). Что и говорить об обычном мусорном пакете, и так содержащем в себе 
все необходимые вещества.

![обыкновенная ночная моль](illustrations/city-life/mole.jpg "Обыкновенная ночная моль")

_Рис. 5. Обыкновенная ночная моль._

Итак, в пакете зарождается обыкновенная ночная моль. Вглядитесь в её лицо (рис. 5). 
Не раз и не два заглядывала она в ваши окна, а вы даже не заметили этого порыва. 
Всеми отвергаема, ночная моль отчаивается, забивается в какую-нибудь тесную щель или под 
корягу, вьёт там кокон и превращается в личинку сизого голубя.

Никто не видел маленького голубя. А всё почему? Потому что личинка голубя, неприглядное 
на вид червеобразное существо, снабжённое густым серым мехом, так и сидит себе в 
сухом тёплом месте, питаясь мелким мусором и старыми газетами. Но приходит и её срок: 
личинка окостеневает, теряет подвижность и превращается в куколку голубя. Все мы не 
раз встречали на чердаке странные объекты, похожие на засушенных голубей. Так вот, 
не стоит тревожить бедолаг, через время защитная оболочка, внешне напоминающая гнилые 
перья, лопнет, и на белый свет взглянет взрослый половозрелый голубь. Конечно, поначалу 
крылья его мягкие и прозрачные, но всего через несколько часов они наливаются густым 
голубинным соком и становятся неотличимы от настоящих.

Голуби по праву считаются самым вездесущим (не только) компонентом городской фауны. 
Кто только от них ни страдает: дворникам приходится отскребать плоские тушки от 
мостовой, домохозяйки ворчат, заметивши на белоснежных простынях небрежный и 
размашистый автограф очередного пернатого, прохожые чертыхаются, почувствовав 
кожею головы лёгкий шлепок, а затем и характерное ощущение сырости. От летающих 
кишечников страдают и памятники архитектуры, т.е. едкий голубиный помёт с 
лёгкостью проедает мрамор, гранит или какой-нибудь там алебастр.

И только маленькие ребятишки, ещё не вкусившие плода познания, всегда радуются, 
завидев стаю пуховых красавцев. Ведь это обещает им увлекательную игру 
"догони голубя" с возможным призом в виде струи помёта в лицо.

![Жизненный цикл](illustrations/city-life/pigeon-cycle.jpg "Жизненный цикл")

_Рис. 6. Жизненный цикл._

Но что же с жизненным циклом? Конечно, на голубях он не останавливается. Каждый 
неоднократно замечал, как поразительно совпадают места обитания бомжей и голубей. 
Понятное дело, что это происходит неспроста. Автор неоднократно наблюдал 
неопрятные копошащиеся комки, образующиеся при высыпании слишком большого 
количества корма на поверхность. Имея хоть толику воображения, легко разглядеть 
у такого комка конечности, головной отросток и подобие лица на нём. Мы не 
знаем, как и где это происходит (скорее всего, в глубинах теплотрассы), 
но представляется очевидным, что наиболее крупные и сложно оформленные 
конкреции голубей обретают сознание и продолжают свою жизнь уже в виде бомжей.

В пользу данной теории говорят следующие факты: бомжи слабо владеют человеческой 
речью (т.к. ещё не отвыкли от голубиного языка, который у них конечно же есть), 
бомжи вьют огромные вонючие гнёзда, бомжи относительно всеядны (доходит и до 
каннибализма, как у голубей), невоздержанны в половых связях, склонны к спонтанным 
миграциям. Да и в целом в их поведении хорошо заметны голубиные повадки.

Что же происходит дальше с этими комками плоти, лишь по недомыслию называемыми 
особями? У них есть два пути. Либо бомжа, мирно спящего в теплоцентрали, 
понемногу съедят крысы (а едят они очень нежно и деликатно своими острыми зубками). 
Либо плоть исчерпывает свой лимит прочности и распадается на различные фрагменты. 
В которых, если их не тревожить, заводятся многочисленные серые мохнатые бабочки 
ночной моли. Таким образом замыкается жизненный цикл этих странных, 
но в чём-то прекрасных существ.

## Эпилог

В завершении рассказа хотелось бы приободрить моего читателя. Если вдруг вам покажется,
что вы потерялись в городе или вообще в жизни, то просто вспомните, что вы в походе. 
А осознав это, продолжайте двигаться к следующей точке маршрута, соблюдая все 
вышеперечисленные рекомендации. Мой рассказ окончен, но путешествие продолжается.

***
##### Список использованной литературы:

1. <a name="1"></a> Gil Sharon. Commensal bacteria play a role in mating preference of Drosophila melanogaster. Proc Natl Acad Sci USA. 2010 Nov 16; 107(46): 20051–20056. 
2. <a name="2"></a> [https://www.nkj.ru/news/26711/](https://www.nkj.ru/news/26711/)
3. <a name="3"></a> В. Похлёбкин "Тайны хорошей кухни"
4. <a name="4"></a> С. Голубицкий "Гидродинамика людских масс"
5. <a name="5"></a> Paul F. Lurquin. The origins of life and the universe. — Columbia University Press, 2003. — p. 96—99

