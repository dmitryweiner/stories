# Оглавление

## Рассказы

1. [Курс выживания туриста в городской тайге](city-life.html)
2. [Детские воспоминания](memories.html)
3. [Модуль 645](unit-645.html)
4. [Мясушко](myasushko.html)
5. [Много старых рассказов](http://dmw2k.chat.ru/obsky/index.htm)
6. [О так называемой реальности](r.htm)
7. [Размышления сервисного модуля](cons.html)
8. [Шпаргалка](cheatsheet.html)

## Программирование

1. [Список дел на React.js + Redux.js](http://todo.dweiner.ru/) [(код)](https://github.com/dmitryweiner/todo-react)
1. [Секьюрный чат](http://secure-chat.dweiner.ru/) [(код)](https://github.com/dmitryweiner/secure-chat-nodejs)
2. Развлечения с React.js
  * [Светофор](http://dweiner.ru/react.html)  
  * [Счётчик секунд](http://dweiner.ru/seconds.html) 
3. [Игра про разработчика](http://bugs.dweiner.ru/) [(код)](https://github.com/dmitryweiner/what-the-bug)
4. [Игра про машинку](http://olegg.500mb.net/car-game.html) [(код)](https://github.com/dmitryweiner/car-game)
5. [Датчик погоды в отдельно взятой комнате](http://sensor.dweiner.ru/) [(код)](https://github.com/dmitryweiner/sensor-data-saver)

## Список рекомендованной литературы

1. Крис Фрит "Мозг и душа"
2. Оливер Сакс "Человек, который принял жену за шляпу"
3. Питер Уоттс "Ложная слепота", "Эхопраксия"
4. Лю Цысинь "Задача трёх тел"
5. Ханну Райаниеми "Квантовый вор", "Фрактальный принц"
6. Рейнольдс Аластер "Город бездны"
7. Энди Уир (Andy Weir) "Марсианин"
8. Алекс Андреев "Паутина", "2048"
9. Дэн Симмонс "Флешбек"
10. Нил Стивенсон "Алмазный век"
11. Майкл Крайтон "Штамм «Андромеда»"
12. Хол Клемент "Экспедиция «Тяготение»"
13. Марк Льюис "Биология желания. Зависимость – не болезнь"
